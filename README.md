# ZSH Theme - Brutus

A simple [oh-my-zsh](https://ohmyz.sh/) theme.
Probably better on a dark background.

## Install

```shell
curl -O https://bitbucket.org/brutusdmc/zsh-theme-brutus/raw/master/brutus.zsh-theme
```

## Prompt

### 1st Line

- Starts with `#` on first line:
- current _shell level_
- _user_ `@` _machine_
- `in` _current directory_
- if in _git_ repo:
  - `on` branch
  - branch state:
    - clean (`√`)
    - dirty (`o`)
- if in virtual environment:
  - `(` virtualenv name `)`
- time (`hh:mm:ss`)
- if return code from last command `!=0`:
  - `→ ` return code

### 2nd Line

- user level:
  - root (`#`)
  - user (`%`)

![Example Prompt](example.png)
