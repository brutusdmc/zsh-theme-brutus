# ZSH Theme Brutus
#
# E.g.
#
# 1 brutus@tpad in …/roles/node_exporter on mybranch-… o (venv) 11:08:43 → 2

ZSH_THEME_GIT_PROMPT_PREFIX="%F{white}on %F{cyan}%-32>…>"
ZSH_THEME_GIT_PROMPT_SUFFIX="%f "
ZSH_THEME_GIT_PROMPT_DIRTY="%<< %F{red}o"
ZSH_THEME_GIT_PROMPT_CLEAN="%<< %F{green}√"

ZSH_THEME_VIRTUALENV_PREFIX="(%F{yellow}%12>…>"
ZSH_THEME_VIRTUALENV_SUFFIX="%<<%f) "

PROMPT="
%F{white}#%f \
%(2L|%F{magenta}|)%L%f \
%(#|%F{red}|%F{cyan})%n%f\
%F{white}@%F{green}%m%f \
%F{white}in %F{yellow}%(5~|…/%4~|%~)%f \
\$(git_prompt_info)\
\$(virtualenv_prompt_info)\
%F{white}%*%f \
%(?||%F{magenta}→ %f%F{red}%?%f)
%(#|%F{red}|%F{cyan})%#%F{white} \
"
